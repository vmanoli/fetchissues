defmodule Issues.CLI do
  @default_count 4
  @moduledoc """
  Handle the command line parsing and the dispatch to
  the various functions that end up generating a
  table of the last _n_ issues in a github project
  Issues.CLI.run(["elixir-lang","elixir"])
  """
  def main(argv) do
    argv
    |> parse_args
    |> process
  end

  @doc """
  `argv` can be -h or --help, which returns :help.
  Otherwise it is a github user name, project name, and (optionally)
  the number of entries to format.
  Return a tuple of `{ user, project, count }`, or `:help` if help was given.
  """
  def parse_args(argv) do
    OptionParser.parse(argv, switches: [help: :boolean], aliases: [h: :help])
    |> elem(1)
    |> args_to_tuple()
  end

  defp args_to_tuple([user, project, count]) do
    {user, project, String.to_integer(count)}
  end

  defp args_to_tuple([user, project]) do
    {user, project, @default_count}
  end

  defp args_to_tuple(_) do
    :help
  end

  @doc """
  Process shows help or fetches from github
  """
  def process(:help) do
    IO.puts("""
    usage: issues <user> <project> [ count | #{@default_count} ]
    @author vicky
    """)

    System.halt(0)
  end

  def process({user, project, count}) do
    Issues.GithubIssues.fetch(user, project)
    |> decode_response()
    |> sort_into_descending_order()
    |> latest(count)
    |> format_to_table()
  end

  def decode_response({:ok, body}), do: body
  def decode_response({:error, error_body}) do
    IO.puts "Error fetching from Github: #{error_body["message"]}"
    System.halt(2)
  end

  def sort_into_descending_order(list_of_issues) do
    list_of_issues
    |> Enum.sort(fn i1, i2 ->
      i1["created_at"] >= i2["created_at"]
      end)
  end

  def latest(sorted_list, count) do
    sorted_list
    |> Enum.take(count)
    |> Enum.reverse
  end

  def format_to_table(issues_list) do
    print_table_headers()
    print_table_body(issues_list)
  end

  def print_table_headers() do
    IO.puts " #  | created_at           | title"
    IO.puts "----+----------------------+-----------------------------------------"
  end

  def print_table_body(list) do
    Enum.map(list, &print_table_row/1)
  end

  def print_table_row(issue) do
    "#{issue["number"]} | #{issue["created_at"]} | #{issue["title"]}"
    |> IO.puts
  end
end
