defmodule Issues.MixProject do
  use Mix.Project

  def project do
    [
      app: :issues,
      name: "My Issues",
      version: "0.1.11",
      source_url: "https://bitbucket.org/vmanoli/fetchissues",
      elixir: "~> 1.6",
      escript: escript_config(),
      start_permanent: Mix.env() == :dev,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0.0"},
      {:poison, "~> 3.1"},
      {:ex_doc, "~> 0.18.1"},
    ]
  end

  #provide configuration for escript (Erlangs utility for running scripts on the command line)
  defp escript_config do
    [
      main_module: Issues.CLI
    ]
  end
end
