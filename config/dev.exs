use Mix.Config

config :issues,
      easter_egg: "Easter egg"

config :logger,
      compile_time_purge_level: :info